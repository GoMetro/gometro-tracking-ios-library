// swift-tools-version:5.3

import PackageDescription

let package = Package(
  name: "GoMetroTracking",
  platforms: [
    .iOS(.v11)
  ],
  products: [
    .library(
      name: "GoMetroTracking",
      targets: ["GoMetroTrackingTarget"]),
  ],
  dependencies: [
    .package(
      name: "Alamofire",
      url: "https://github.com/Alamofire/Alamofire.git",
      .upToNextMajor(from: "5.4.1")),
    .package(
      name: "KeychainSwift",
      url: "https://github.com/evgenyneu/keychain-swift.git",
      .upToNextMajor(from: "19.0.0")),
    .package(
      name: "JWTDecode",
      url: "https://github.com/auth0/JWTDecode.swift.git",
      .upToNextMajor(from: "2.5.0")),
    .package(
      name: "DeviceKit",
      url: "https://github.com/devicekit/DeviceKit.git",
      .upToNextMajor(from: "4.2.1"))
  ],
  targets: [
    .binaryTarget(
      name: "GoMetroTracking",
      path: "Sources/GoMetroTracking.xcframework"),
    .target(
      name: "GoMetroTrackingTarget",
      dependencies: [
        .target(name: "GoMetroTracking"),
        "Alamofire",
        "KeychainSwift",
        "JWTDecode",
        "DeviceKit"
      ],
      linkerSettings: [
        .linkedFramework("CoreLocation"),
        .linkedFramework("CryptoKit")
      ])
  ],
  swiftLanguageVersions: [.v5]
)
