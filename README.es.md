# GoMetro iOS SDK

## Integración

### Swift Package Manager

En Xcode, navega a _File_ > _Swift Packages_ > _Add Package Dependency..._ e introduce la dirección de Git address del repositorio (https://bitbucket.org/GoMetro/gometro-tracking-ios-library.git).

Selecciona la versión que quieres incluir y continúa con el diálogo de instalación.

#### Swift 5.2

Si estás utilizando Swift 5.2, necesitarás integrar el archivo `.xcframework` manualmente a tu proyecto de Xcode. Para hacerlo, descarga el repositorio de Git y arrastra el archivo hacia tu workspace de Xcode.

También necesitarás agregar las dependencias del framework manualmente (incluyendo la librería de `CoreLocation`) utilizando Swift Package Manager. Revisa el archivo `Package.swift` para obtener la lista de dependencias a agregar.

### Info.plist

Agrega las siguientes entradas:

* `NSLocationAlwaysAndWhenInUseUsageDescription`: Un string que explique al usuario por qué estás solicitando acceso a su ubicación
* `NSLocationAlwaysUsageDescription`: Un string que explique al usuario por qué estás solicitando acceso a su ubicación mientras la aplicación esté en segundo plano
* `NSLocationWhenInUseUsageDescription`: Un string que explique al usuario por qué estás solicitando acceso a su ubicación mientras la aplicación esté en uso
* `NSLocationTemporaryUsageDescriptionDictionary`: Un diccionaro. Agrega una llave de nombre `GoMetroTracking` y asigna como valor un string que explique al usuario por qué necesitas alta precisión al solicitar su ubicación
* `NSMotionUsageDescription`: Un string que explique al usuario por qué estás solicitando datos de movimiento

### Capabilities

En Xcode, navega a la configuración de Targets para tu proyecto. Bajo _Signing & Capabilities_ > _Background Modes_, habilita la opción de _Location updates_.

## Uso

### Configuración inicial

En el _delegate_ de tu aplicación, agrega el siguiente código para que sea ejecutado cuando el usuario abra tu app por primera vez.

```swift
func application(_ application: UIApplication,
                 didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
  GoMetroTracking.initialise(clientId: "<gometro-client-id>",
                             clientSecret: "<gometro-client-secret>",
                             externalDeviceIdentifier: "<device-uuid>")
  // ...
  return true
}
```

El parámetro `externalDeviceIdentifier` debe ser un string UUID que identifique de manera única a este dispositivo ante los servidores de GoMetro. Por razones de privacidad, Apple solamente permite la asignación de identificadores únicos durante la misma instalación de la aplicación (es decir, si tu aplicación es eliminada y reinstalada por el usuario, es necesario generar un nuevo UUID). Por lo tanto, recomendamos que crees un valor usando la clase `UUID()` y que lo almacenes en `UserDefaults` para que, en caso de que el usuario desinstale la aplicación, el valor sea eliminado.

Después de hacer esto, la mayoría de los mecanismos de recolección de datos del SDK serán activados **con excepción del rastreo de ubicación**.

### Rastreo de ubicación en segundo plano

Debido al modelo de permisos de ubicación de iOS, el SDK tiene que separar el rastreo de _ubicación en segundo plano_ del rastreo de _ubicación precisa_.

* El rastreo de _ubicación en segundo plano_ se usa cuando el usuario NO está viajando de un origen a un destino en la ciudad.
* El rastreo de _ubicación precisa_ debe ser activado una vez que el usuario haya comenzado un viaje.

En el momento en que tu aplicación sea abierta, llama al siguiente método para activar el monitoreo de ubicación en segundo plano. Esto comenzará el rastreo de baja precisión del usuario mientras no esté moviéndose en algún medio de transporte. Este método solicitará  permisos de ubicación.

```swift
GoMetroTracking.startBackgroundLocationMonitoring()
```

Ya que esto esté activo, debes incentivar a tus usuarios a activar el rastreo de ubicación precisa una vez que hayan comenzado un viaje.

### Notificaciones de cambio significativo de ubicación

Para ayudar en este punto, el SDK emite una notificación interna a tu aplicación cuando el dispositivo se ha movido una distancia significativa (indicando que el usuario probablemente se esté transportando de un punto a otro en la ciudad) para que puedas desplegar una notificación push, una alerta, o alguna otra llamada de atención incentivando al usuario a habilitar el rastreo de _ubicación precisa_.

Para escuchar esta notificación, agrega este código:

```swift
NotificationCenter.default.addObserver(self,
  selector: #selector(didMoveSignificantLocation(notification:)),
  name: .GoMetroDeviceDidChangeSignificantLocationInBackground,
  object: nil)
```

Este es un ejemplo de cómo puedes desplegar una notificación local al usuario cuando esto sucede:

```swift
@objc private func didMoveSignificantLocation(notification: Notification) {
  if UIApplication.shared.applicationState == .background {
    let notificationCenter = UNUserNotificationCenter.current()
    notificationCenter.getNotificationSettings { (settings) in
      if settings.authorizationStatus == .authorized {
        let content = UNMutableNotificationContent()
        content.title = "Tu ubicación ha cambiado"
        content.body = "Parece que te estás transportando pero no has habilitado el rastreo."
        content.sound = UNNotificationSound.default
        let request = UNNotificationRequest(identifier: "locationChanged",
                                            content: content,
                                            trigger: nil)
        notificationCenter.add(request)
      }
    }
  }
}
```

### Rastreo de ubicación precisa

Para activar el rastreo de ubicación precisa, llama a este método:

```swift
GoMetroTracking.startTracking()
```

Para desactivar:

```swift
GoMetroTracking.stopTracking()
```

Debes desactivar el rastreo preciso cuando el usuario termine su viaje, de lo contrario, tu aplicación puede ocasionar un consumo desmedido de batería aumentando la posibilidad de que el usuario elimine la aplicación o limite sus permisos de ubicación. Para facilitar esto, el SDK emite una notificación cuando el usuario ha llegado a un lugar destino (`.GoMetroDeviceDidArriveAtDestination`). Puedes suscribirte a esta notificación para decidir si es necesario desactivar el rastreo preciso (similar al ejemplo de notificación anterior).
