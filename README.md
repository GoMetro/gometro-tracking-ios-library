# GoMetro iOS SDK

## Integration

### Swift Package Manager

In Xcode, go to _File_ > _Swift Packages_ > _Add Package Dependency..._ and enter the Git address for this repository (https://bitbucket.org/GoMetro/gometro-tracking-ios-library.git).

Select the version you want to include and finish the setup dialog.

#### Swift 5.2

If you're using Swift 5.2, you will need to integrate the `.xcframework` file manually into your Xcode project. To do so, download the Git repository for the library and drag and drop the file into your Xcode workspace.

You will also need to manually add the framework's dependencies (including the `CoreLocation` framework) into your project using the Swift Package Manager. Look into the `Package.swift` file for a list of dependencies.

### Info.plist

Add the following keys:

* `NSLocationAlwaysAndWhenInUseUsageDescription`: A string that explains to the user why you're requiring their location
* `NSLocationAlwaysUsageDescription`: A string that explains why you require their location while the app is in the background
* `NSLocationWhenInUseUsageDescription`: A string that explains why you require their location while the app is in use
* `NSLocationTemporaryUsageDescriptionDictionary`: A dictionary. Add a key with the name `GoMetroTracking` and set a string value that explains why you require their location to be precise
* `NSMotionUsageDescription`: A string that explains to the user why you're requiring their motion data

### Capabilities

In Xcode, go to your project's Target configurations. Under _Signing & Capabilities_ > _Background Modes_, check the box next to _Location updates_.

## Usage

### Initialisation

In your application's _delegate_, add the following code so it is executed during application launch.

```swift
func application(_ application: UIApplication,
                 didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
  GoMetroTracking.initialise(clientId: "<gometro-client-id>",
                             clientSecret: "<gometro-client-secret>",
                             externalDeviceIdentifier: "<device-uuid>")
  // ...
  return true
}
```

The `externalDeviceIdentifier` should be a UUID string that identifies this device uniquely towards the GoMetro SDK servers. For privacy reasons, Apple only allows unique device identifiers to be set during the same app installation (meaning if your app is deleted and reinstalled, you should generate a new UUID). Because of this, we recommend that you create a value using the `UUID()` class and store it in `UserDefaults` so it is wiped out if the user uninstalls your application.

After doing this, most of the SDK's data collection mechanisms will be activated **except for location tracking**.

### Background location monitoring

Because of iOS's location permissions model, the SDK has to separate _background location_ tracking from _precise location_ tracking.

* _Background location_ tracking is used when the user is NOT traveling from one destination to the other using multi-modal transportation.
* _Precise location_ tracking should be activated once the user starts a trip.

After your app is launched, call the following method to activate background location monitoring. This will make sure we track the user's location with low precision when they're not really moving on transportation. This method will request the user's location permissions.

```swift
GoMetroTracking.startBackgroundLocationMonitoring()
```

Once this is active, you should encourage your users to activate precise location tracking when starting a trip.

### Significant location change notifications

To help with this task, the SDK can emit an internal notification to your app when the device has moved a significant distance (meaning the user is probably moving from one place in the city to another) so your application can display a push notification, an alert, or other call to action to encourage the user to turn on _precise location_ tracking.

To listen for this notification, add this code to your app:

```swift
NotificationCenter.default.addObserver(self,
  selector: #selector(didMoveSignificantLocation(notification:)),
  name: .GoMetroDeviceDidChangeSignificantLocationInBackground,
  object: nil)
```

This is an example on how you can display a local notification to the user when this happens:

```swift
@objc private func didMoveSignificantLocation(notification: Notification) {
  if UIApplication.shared.applicationState == .background {
    let notificationCenter = UNUserNotificationCenter.current()
    notificationCenter.getNotificationSettings { (settings) in
      if settings.authorizationStatus == .authorized {
        let content = UNMutableNotificationContent()
        content.title = "Location changed"
        content.body = "It looks like you're moving but you haven't started tracking."
        content.sound = UNNotificationSound.default
        let request = UNNotificationRequest(identifier: "locationChanged",
                                            content: content,
                                            trigger: nil)
        notificationCenter.add(request)
      }
    }
  }
}
```

### Precise location monitoring

To activate precise location monitoring, call this method:

```swift
GoMetroTracking.startTracking()
```

To stop:

```swift
GoMetroTracking.stopTracking()
```

You should stop precise tracking when the user ended their trip, otherwise your app can create a big battery drain in your user's device and you run the risk of having them revoke location permissions. To facilitate this, the SDK emits a notification when a user has arrived to a certain location (`.GoMetroDeviceDidArriveAtDestination`). You can listen for this notification in order to decide if you should stop precise tracking (similar to the notification example above).
